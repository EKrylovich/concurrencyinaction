package com.concurrency.chapters.introduction;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SequenceTest {

    @Test
    public void safe() throws InterruptedException {
//        Given
        final SequenceRunner safeSequenceRunner = new SequenceRunner(new SafeSequence());
//        When
        final int result = safeSequenceRunner.proceed();
//        Then
        assertEquals(100_000, result);
    }

    @Test
    public void unSafe() throws InterruptedException {
//        Given
        final SequenceRunner unSafeSequenceRunner = new SequenceRunner(new UnsafeSequence());
//        When
        final int result = unSafeSequenceRunner.proceed();
//        Then
        assertNotEquals(100_000, result);

    }
}
