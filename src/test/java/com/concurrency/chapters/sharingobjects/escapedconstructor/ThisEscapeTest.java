package com.concurrency.chapters.sharingobjects.escapedconstructor;

import org.junit.Test;

public class ThisEscapeTest {

    @Test
    public void testConstructorEscape(){
        EventSource eventSource = new EventSource();
        eventSource.start();
        while (true) {
            new ThisEscape(eventSource);
        }
    }
}
