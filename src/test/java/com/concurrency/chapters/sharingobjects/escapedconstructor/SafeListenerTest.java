package com.concurrency.chapters.sharingobjects.escapedconstructor;

import org.junit.Test;

public class SafeListenerTest {

    @Test
    public void testSafety(){

        EventSource eventSource = new EventSource();
        eventSource.start();
        while (true) {
            SafeListener.newInstance(eventSource);
        }
    }
}
