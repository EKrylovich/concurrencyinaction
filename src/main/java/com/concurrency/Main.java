package com.concurrency;

import com.concurrency.chapters.introduction.SafeSequence;
import com.concurrency.chapters.introduction.UnsafeSequence;
import com.concurrency.chapters.introduction.SequenceRunner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        final SequenceRunner sequenceRunner = new SequenceRunner(new UnsafeSequence());
        final SequenceRunner safeSequenceRunner = new SequenceRunner(new SafeSequence());
        sequenceRunner.proceed();
        safeSequenceRunner.proceed();
    }
}
