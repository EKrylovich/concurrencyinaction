package com.concurrency.tech;

public @interface GuardedBy {

    String value();
}
