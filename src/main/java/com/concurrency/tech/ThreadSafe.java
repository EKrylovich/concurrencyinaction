package com.concurrency.tech;

public @interface ThreadSafe {
    String value() default "";
}
