package com.concurrency.tech;

public @interface NotThreadSafe {
    String value();
}
