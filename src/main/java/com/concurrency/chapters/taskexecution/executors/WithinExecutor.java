package com.concurrency.chapters.taskexecution.executors;

import java.util.concurrent.Executor;

public class WithinExecutor implements Executor {
    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
