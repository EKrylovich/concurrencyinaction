package com.concurrency.chapters.taskexecution.webservers;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

//import static com.sun.activation.registries.LogSupport.log;

public class LifecycleWebServer {


    private static final int NTHREADS = 100;
    private static final ExecutorService exec = Executors.newFixedThreadPool(NTHREADS);

    public void start() throws IOException {
        final ServerSocket serverSocket = new ServerSocket(80);
        while(exec.isShutdown()) {
            try {
                final Socket connection = serverSocket.accept();
                exec.execute(() -> handleRequest(connection));
            } catch (RejectedExecutionException exp) {
                if (!exec.isShutdown()){
                    System.out.println("task submition rejected");
                }
            }
        }
    }

    public void stop() {
        exec.shutdown();
    }


    private void handleRequest(Socket connection) {
        final Request request = readRequest(connection);
        if (isShutdownRequest(request)) {
            stop();
        } else {
            dispatchRequest(request);
        }
    }

    private void dispatchRequest(Request request) {

    }

    private static boolean isShutdownRequest(Request request) {
        return false;
    }

    private static Request readRequest(Socket connection) {
        return null;
    }

    private static class Request {
    }
}
