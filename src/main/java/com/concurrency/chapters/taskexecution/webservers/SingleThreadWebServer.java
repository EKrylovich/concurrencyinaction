package com.concurrency.chapters.taskexecution.webservers;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SingleThreadWebServer {

    public static void main(final String[] args) throws IOException {
        final ServerSocket serverSocket = new ServerSocket(80);
        while(true) {
            final Socket connection = serverSocket.accept();
            handleRequest(connection);
        }
    }

    private static void handleRequest(Socket connection) {
//        ....
        }
}
