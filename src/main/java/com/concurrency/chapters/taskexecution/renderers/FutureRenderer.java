package com.concurrency.chapters.taskexecution.renderers;

import com.sun.scenario.effect.ImageData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class FutureRenderer {

    private static final int NTHREADS = 100;
    private static final ExecutorService exec = Executors.newFixedThreadPool(NTHREADS);

    void renderPage(final CharSequence source) {
        final List<ImageInfo> imageInfos = scanForImageInfo(source);
        final Callable<List<ImageData>> task = downloadImages(imageInfos);
        final Future<List<ImageData>> future = exec.submit(task);
        renderText(source);
        try {
            final List<ImageData> imageData = future.get();
            for (ImageData data : imageData){
                renderImage(data);
            }
        } catch (InterruptedException e) {
            // re-assert the thread's interrupted status
            Thread.currentThread().interrupt();
            //We don't need result
            future.cancel(true);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    Callable<List<ImageData>> downloadImages(List<ImageInfo> imageInfos){
        return () -> {
            List<ImageData> result = new ArrayList<>();
            for (ImageInfo info : imageInfos){
                result.add(info.downloadImage());
            }
            return result;
        };
    }

    private void renderImage(ImageData data) {

    }

    private void renderText(CharSequence source) {

    }

    private List<ImageInfo> scanForImageInfo(CharSequence source) {
        return null;
    }

    private class ImageInfo {
        public ImageData downloadImage() {
            return null;
        }
    }
}
