package com.concurrency.chapters.taskexecution.renderers.queue;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Renderer {
    private static final int NTHREADS = 100;
    private static final ExecutorService exec = Executors.newFixedThreadPool(NTHREADS);

    void renderPage(final CharSequence source) {
        final List<ImageInfo> info = scanForImageInfo(source);
        CompletionService<ImageData> completionService =
                new ExecutorCompletionService<>(exec);
        for (final ImageInfo imageInfo : info)
            completionService.submit(imageInfo::downloadImage);

        renderText(source);

        try {
            for (int t = 0, n = info.size(); t < n; t++) {
                Future<ImageData> future = completionService.take();
                ImageData imageData = future.get();
                renderImage(imageData);
            }
        } catch (InterruptedException e) {
            // re-assert the thread's interrupted status
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    Callable<List<ImageData>> downloadImages(List<ImageInfo> imageInfos){
        return () -> {
            List<ImageData> result = new ArrayList<>();
            for (ImageInfo info : imageInfos){
                result.add(info.downloadImage());
            }
            return result;
        };
    }

    private void renderImage(ImageData data) {

    }

    private void renderText(CharSequence source) {

    }

    private List<ImageInfo> scanForImageInfo(CharSequence source) {
        return null;
    }

    private class ImageInfo {
        public ImageData downloadImage() {
            return null;
        }
    }
}
