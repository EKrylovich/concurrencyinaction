package com.concurrency.chapters.taskexecution.renderers;


import java.util.ArrayList;
import java.util.List;

public class SingleThreadRenderer {

    void renderPage(final CharSequence source){
        renderText(source);
        final List<ImageData> imageData = new ArrayList<>();
        for (ImageInfo imageInfo : scanForImageInfo(source)){
            imageData.add(imageInfo.downloadImage());
        }
        for (ImageData data : imageData) {
            renderImage(data);
        }
    }

    private void renderImage(ImageData data) {

    }

    private ImageInfo[] scanForImageInfo(CharSequence source) {
        return new ImageInfo[0];
    }

    private void renderText(CharSequence source) {

    }

    private class ImageData {
    }

    private class ImageInfo {
        public ImageData downloadImage() {
            return null;
        }
    }
}
