package com.concurrency.chapters.cancelshutdown.generator;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BrokenPrimeProducer extends Thread {
    private final BlockingQueue<BigInteger> queue;
    private volatile boolean cancelled;

    public BrokenPrimeProducer(BlockingQueue<BigInteger> queue) {
        this.queue = queue;
    }

    public void run(){
        try {
            BigInteger prime = BigInteger.ONE;
            while (!cancelled){
                queue.put(prime = prime.nextProbablePrime());
            }
        } catch (InterruptedException consumed) {

        }
    }

    public void cancel(){
        this.cancelled = true;
    }



    //Usage
    public static void main(String[] args) throws InterruptedException {
        final BlockingQueue<BigInteger> primes = new ArrayBlockingQueue<>(20);
        final BrokenPrimeProducer primeProducer = new BrokenPrimeProducer(primes);
        primeProducer.start();

        try {
            while (needMorePrimes()){
                consume(primes.take());
            }
        } finally {
            primeProducer.cancel();
        }
    }

    private static void consume(BigInteger take) {
        System.out.println(take);
    }

    private static boolean needMorePrimes() {
        return new Random().nextBoolean();
    }
}
