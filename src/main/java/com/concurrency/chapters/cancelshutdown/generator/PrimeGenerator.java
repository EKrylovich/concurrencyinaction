package com.concurrency.chapters.cancelshutdown.generator;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

@ThreadSafe
public class PrimeGenerator implements Runnable {

    @GuardedBy("this")
    private final List<BigInteger> primes = new ArrayList<>();

    private volatile boolean cancelled;


    @Override
    public void run() {
        BigInteger prime = BigInteger.ONE;

        while (!cancelled){
            prime = prime.nextProbablePrime();
            synchronized (this) {
                primes.add(prime);
            }
        }
    }

    public void cancel(){
        this.cancelled = true;
    }

    public List<BigInteger> getPrimes() {
        return new ArrayList<>(primes);
    }


    //Usage

    public static void main(String[] args) throws InterruptedException {
        final PrimeGenerator primeGenerator = new PrimeGenerator();
        new Thread(primeGenerator).start();
        try {
            SECONDS.sleep(1);
        } finally {
            primeGenerator.cancel();
        }

        System.out.println(primeGenerator.getPrimes());
    }
}
