package com.concurrency.chapters.cancelshutdown.generator;

import java.math.BigInteger;
import java.util.concurrent.BlockingQueue;

public class PrimeProducer extends Thread {
    private final BlockingQueue<BigInteger> queue;

    public PrimeProducer(BlockingQueue<BigInteger> queue) {
        this.queue = queue;
    }

    public void run(){
        try {
            BigInteger prime = BigInteger.ONE;
            while (!Thread.currentThread().isInterrupted()){
                queue.put(prime = prime.nextProbablePrime());
            }
        } catch (InterruptedException consumed) {

        }
    }

    public void cancel(){
        interrupt();
    }
}
