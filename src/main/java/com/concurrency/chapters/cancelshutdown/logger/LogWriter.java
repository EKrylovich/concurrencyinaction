package com.concurrency.chapters.cancelshutdown.logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LogWriter {
    private final BlockingQueue<String> queue;
    private final LoggerThread logger;

    private volatile boolean shutDownRequested;

    public LogWriter(final PrintWriter writer) {
        this.queue = new LinkedBlockingQueue<>();
        this.logger = new LoggerThread(writer);
    }

    public void start(){
        logger.start();
    }

    public void log(final String msg) throws InterruptedException{
        queue.put(msg);
    }

    public void logWithCondition(final String msg) throws InterruptedException{
        if (!shutDownRequested) {
            queue.put(msg);
        } else {
            throw new IllegalStateException("Logger is shut down");
        }
    }

    private class LoggerThread extends Thread{

        private final PrintWriter writer;

        public LoggerThread(PrintWriter writer) {
            this.writer = writer;
        }

        public void run(){
            try {
                while (true){
                    writer.println(queue.take());
                }
            } catch (InterruptedException ignore) {
            } finally {
                writer.close();
            }
        }
    }
}
