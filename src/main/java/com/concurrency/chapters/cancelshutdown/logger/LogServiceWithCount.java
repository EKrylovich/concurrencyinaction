package com.concurrency.chapters.cancelshutdown.logger;

import com.concurrency.tech.GuardedBy;

import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LogServiceWithCount {
    private final BlockingQueue<String> queue;
    private final LoggerThread logger;

    private final PrintWriter writer;
    @GuardedBy("this") private boolean isShutDown;
    @GuardedBy("this") private int reservations;


    public LogServiceWithCount(final PrintWriter writer) {
        this.writer = writer;
        this.queue = new LinkedBlockingQueue<>();
        this.logger = new LoggerThread();
    }

    public void start(){
        logger.start();
    }

    public void log(final String msg) throws InterruptedException{
        synchronized (this){
            if (isShutDown){
                throw new IllegalStateException("Service shut down");
            }
            ++reservations;
        }
        queue.put(msg);
    }

    public void stop(){
        synchronized (this){
            isShutDown = true;
        }
        logger.interrupt();
    }

    private class LoggerThread extends Thread{

        public void run(){
            try {
                while (true){
                    synchronized (LogServiceWithCount.this){
                        if (isShutDown && reservations == 0){
                            break;
                        }
                    }
                    final String msg = queue.take();

                    synchronized (LogServiceWithCount.this) {
                        --reservations;
                    }

                    writer.println(msg);
                }
            } catch (InterruptedException ignore) {
                //retry
            } finally {
                writer.close();
            }
        }
    }
}
