package com.concurrency.chapters.cancelshutdown;

import java.util.concurrent.BlockingQueue;

public class TaskProducer {
    private boolean interrupted = false;

    public Task getNextTask(BlockingQueue<Task> queue) {
        try {
            while (true){
                try {
                    return queue.take();
                } catch (InterruptedException exp) {
                    interrupted = true;
                    //continue
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public static class Task{

    }
}
