package com.concurrency.chapters.composingobjects;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

@ThreadSafe
public class Counter {

    @GuardedBy("this") private long value = 0;

    public synchronized long getValue(){
        return value;
    }

    public synchronized long increment(){
        if (value == Long.MAX_VALUE)
            throw new IllegalStateException("Counter overflow");
        return ++value;
    }


}
