package com.concurrency.chapters.composingobjects.instanceconfinement.delegating;

import com.concurrency.tech.NotThreadSafe;

import java.util.concurrent.atomic.AtomicInteger;

@NotThreadSafe("Two dependent fields")
public class NumberRange {
    //Invariant: lower < upper

    private final AtomicInteger lower = new AtomicInteger(0);
    private final AtomicInteger upper = new AtomicInteger(lower.intValue() + 1);

    public void setLower(final int lowerValue){
        // Warning: unsafe check-then-act
        if (lowerValue > upper.get()) {
            throw new IllegalArgumentException("Can't set lower to " + lowerValue + " > upper");
        }
        lower.set(lowerValue);
    }

    public void setUpper(final int upperValue){
        // Warning: unsafe check-then-act
        if (upperValue < lower.get()) {
            throw new IllegalArgumentException("Can't set upper to " + upperValue + " < lower");
        }
        upper.set(upperValue);
    }

    public boolean isInRange(final int value) {
        return (value >= lower.get() && value <= upper.get());
    }
}
