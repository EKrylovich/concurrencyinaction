package com.concurrency.chapters.composingobjects.instanceconfinement.trackingfleetvehicles.delegating;

import com.concurrency.tech.ThreadSafe;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ThreadSafe
public class DelegatingVehicleTracker {
    private final ConcurrentHashMap<String, Point> locations;
    private final Map<String, Point> unmodifiableMap;

    public DelegatingVehicleTracker(final Map<String, Point> points) {
        this.locations = new ConcurrentHashMap<>(points);
        this.unmodifiableMap = Collections.unmodifiableMap(points);
    }

    public Map<String, Point> getLiveLocations(){
        return unmodifiableMap;
    }

    public Point getLocation(final String id) {
        return locations.get(id);
    }

    public void setLocations(final String id, final int x, final int y){
        if (locations.replace(id, new Point(x, y)) == null) {
            throw new IllegalArgumentException("Invalid vehicle name: " + id);
        }
    }

    public Map<String, Point> getStaticLocations(){
        return Collections.unmodifiableMap(new HashMap<>(locations));
    }
}
