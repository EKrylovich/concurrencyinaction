package com.concurrency.chapters.composingobjects.instanceconfinement.delegating;

import com.concurrency.tech.ThreadSafe;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@ThreadSafe
public class VisualComponent {
    private final List<String> keyListeners = new CopyOnWriteArrayList<>();
    private final List<String> mouseListeners = new CopyOnWriteArrayList<>();

    public void addKeyListener(final String listener) {
        keyListeners.add(listener);
    }

    public void addMouseListener(final String listener) {
        mouseListeners.add(listener);
    }

    public void removeKeyListener(final String listener) {
        keyListeners.remove(listener);
    }

    public void removeMouseListener(final String listener) {
        mouseListeners.remove(listener);
    }
}
