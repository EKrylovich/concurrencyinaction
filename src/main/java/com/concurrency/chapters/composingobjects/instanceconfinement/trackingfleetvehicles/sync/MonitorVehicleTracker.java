package com.concurrency.chapters.composingobjects.instanceconfinement.trackingfleetvehicles.sync;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ThreadSafe
public class MonitorVehicleTracker {

    @GuardedBy("this") private final Map<String, MutablePoint> locations;

    public MonitorVehicleTracker(final Map<String, MutablePoint> locations) {
        this.locations = deepCopy(locations);
    }

    public synchronized Map<String, MutablePoint> getLocations(){
        return deepCopy(locations);
    }

    public synchronized MutablePoint getLocation(final String id) {
        final MutablePoint loc = locations.get(id);
        return loc == null ? null : new MutablePoint(loc);
    }

    public synchronized void setLocations(final String id, final int x, final int y){
        final MutablePoint loc = locations.get(id);
        if (loc == null) {
            throw new IllegalArgumentException("No such id: " + id);
        }
        loc.x = x;
        loc.y = y;
    }


    private static Map<String, MutablePoint> deepCopy(final Map<String, MutablePoint> locations) {
        final Map<String, MutablePoint> result = new HashMap<>();
        for (final String id : locations.keySet()){
            result.put(id, new MutablePoint(locations.get(id)));
        }
        return Collections.unmodifiableMap(result);
    }
}
