package com.concurrency.chapters.composingobjects.instanceconfinement.trackingfleetvehicles.publishing;

import com.concurrency.tech.ThreadSafe;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ThreadSafe
public class PublishingVehicleTracker {
    private final Map<String, SafePoint> locations;
    private final Map<String, SafePoint> unmodifiebleLocations;

    public PublishingVehicleTracker(final Map<String, SafePoint> locations){
        this.locations = new ConcurrentHashMap<>(locations);
        this.unmodifiebleLocations = Collections.unmodifiableMap(locations);
    }

    public Map<String, SafePoint> getLocations(){
        return unmodifiebleLocations;
    }

    public SafePoint getLocation(final String id){
        return locations.get(id);
    }

    public void setLocations(final String id, final int x, final int y){
        if(!locations.containsKey(id)){
            throw new IllegalArgumentException("Invalid vehicle name: " + id);
        }
        locations.get(id).set(x, y);
    }
}
