package com.concurrency.chapters.composingobjects.instanceconfinement;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

import java.util.HashSet;
import java.util.Set;

@ThreadSafe
public class PersonSet {
    @GuardedBy("this") private final Set<Person> mySet = new HashSet<>();

    public synchronized void addPerson(final Person person){
        mySet.add(person);
    }

    public synchronized boolean contains(final Person person) {
        return mySet.contains(person);
    }
}
