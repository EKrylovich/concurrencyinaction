package com.concurrency.chapters.composingobjects.instanceconfinement.trackingfleetvehicles.publishing;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

@ThreadSafe
public class SafePoint {
    @GuardedBy("this") private int x, y;

    private SafePoint(final int[] values){
        this.x = values[0];
        this.y = values[1];
    }

    public SafePoint(final SafePoint safePoint) {
        this(safePoint.get());
    }

    public SafePoint(final int x, final int y){
        this.x = x;
        this.y = y;
    }

    public synchronized int[] get() {
        return new int[]{x, y};
    }

    public synchronized void set(final int x, final int y) {
        this.x = x;
        this.y = y;
    }
}
