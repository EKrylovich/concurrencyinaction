package com.concurrency.chapters.composingobjects.instanceconfinement.trackingfleetvehicles.delegating;

public class Point {
    public final int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
