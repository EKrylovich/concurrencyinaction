package com.concurrency.chapters.composingobjects.instanceconfinement.trackingfleetvehicles.sync;

import com.concurrency.tech.NotThreadSafe;

@NotThreadSafe("")
public class MutablePoint {
    public int x;
    public int y;

    public MutablePoint() {
        this.x = 0;
        this.y = 0;
    }

    public MutablePoint(final MutablePoint loc) {
        this.x = loc.x;
        this.y = loc.x;
    }
}
