package com.concurrency.chapters.composingobjects.instanceconfinement;

import com.concurrency.tech.GuardedBy;

public class PrivateLock {

    private final Object myLock = new Object();

    //In book it's package private but in life it must be private
    //to incapsulate its usage
    @GuardedBy("myLock") Widget widget;

    void someMethod(){
        synchronized (myLock){
            // access or modify the state of widget
        }
    }
}
