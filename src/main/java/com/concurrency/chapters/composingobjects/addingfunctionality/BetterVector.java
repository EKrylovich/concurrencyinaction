package com.concurrency.chapters.composingobjects.addingfunctionality;

import com.concurrency.tech.ThreadSafe;

import java.util.Vector;

// but not so good, because now sync policy is splited between two classes
@ThreadSafe
public class BetterVector<E> extends Vector<E> {

    public synchronized boolean putIfAbsent(final E value){
        boolean absent = !contains(value);
        if (absent){
            add(value);
        }
        return absent;
    }
}
