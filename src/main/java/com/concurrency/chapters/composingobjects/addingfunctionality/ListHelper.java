package com.concurrency.chapters.composingobjects.addingfunctionality;

import com.concurrency.tech.NotThreadSafe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@NotThreadSafe("Different lock objects are used")
public class ListHelper<E> {
    public List<E> list = Collections.synchronizedList(new ArrayList<>());

    public synchronized boolean putIfAbsent(final E value){
        boolean absent = !list.contains(value);
        if (absent){
            list.add(value);
        }
        return absent;
    }
}
