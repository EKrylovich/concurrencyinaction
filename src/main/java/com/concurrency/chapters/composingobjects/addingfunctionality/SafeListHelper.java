package com.concurrency.chapters.composingobjects.addingfunctionality;

import com.concurrency.tech.ThreadSafe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ThreadSafe // but still fragile for similar reasons as BetterVector (extension)
public class SafeListHelper<E> {
    public List<E> list = Collections.synchronizedList(new ArrayList<>());

    public boolean putIfAbsent(final E value){
        synchronized (list) {
            boolean absent = !list.contains(value);
            if (absent) {
                list.add(value);
            }
            return absent;
        }
    }

}
