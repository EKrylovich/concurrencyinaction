package com.concurrency.chapters.introduction;

public class SequenceRunnable implements Runnable {

    private final ISequence sequence;

    public SequenceRunnable(ISequence sequence) {
        this.sequence = sequence;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10_000; i++) {
            sequence.increment();
        }
    }



}
