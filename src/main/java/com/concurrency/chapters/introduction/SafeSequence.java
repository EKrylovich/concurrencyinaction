package com.concurrency.chapters.introduction;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

@ThreadSafe
public class SafeSequence implements ISequence {

    @GuardedBy("this") private int value;

    public synchronized void increment(){
        value++;
    }

    public int getValue(){
        return value;
    }
}
