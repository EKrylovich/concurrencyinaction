package com.concurrency.chapters.introduction;

import com.concurrency.tech.NotThreadSafe;

@NotThreadSafe("Race condition")
public class UnsafeSequence implements ISequence {

    private int value;

    public void increment(){
        value++;
    }

    public int getValue(){
        return value;
    }

}
