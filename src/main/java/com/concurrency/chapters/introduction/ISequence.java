package com.concurrency.chapters.introduction;

public interface ISequence {

    void increment();

    int getValue();
}
