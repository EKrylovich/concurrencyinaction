package com.concurrency.chapters.introduction;

import java.util.ArrayList;
import java.util.List;

public class SequenceRunner {

    private final ISequence sequence;

    public SequenceRunner(ISequence sequence) {
        this.sequence = sequence;
    }

    public int proceed() throws InterruptedException {
        final List<Thread> threads = new ArrayList<>();

        // Variable to increment from multiple threads
        // Run 10 threads to increment the same variable
        for (int i = 0; i < 10; i++) {
            final Thread thread = new Thread(new SequenceRunnable(sequence));
            thread.start();
            threads.add(thread);
        }

        // Wait until all threads are finished
        for (Thread thread : threads) {
            thread.join();
        }

        return sequence.getValue();
    }
}
