package com.concurrency.chapters.sharingobjects.immutatbility;

import jdk.nashorn.internal.ir.annotations.Immutable;

import java.util.Arrays;

@Immutable
public class OneValueCache {

    private final Integer lastNumber;
    private final int[] lastFactors;

    public OneValueCache(final Integer lastNumber, final int[] lastFactors) {
        this.lastNumber = lastNumber;
        this.lastFactors = Arrays.copyOf(lastFactors, lastFactors.length);
    }

    public int[] getFactors(final Integer value){
        return lastNumber == null || !lastNumber.equals(value) ?
                null :
                Arrays.copyOf(lastFactors, lastFactors.length);
    }
}
