package com.concurrency.chapters.sharingobjects.immutatbility;

import javax.servlet.*;
import java.io.IOException;
import java.math.BigInteger;
import java.util.stream.IntStream;

public class VolatileCachedFactorizer implements Servlet {

    private volatile OneValueCache cache = new OneValueCache(null, null);

    @Override
    public void service(final ServletRequest req, final ServletResponse res) throws IOException {
        final Integer value = extractFromRequest(req);
        int[] factors = cache.getFactors(value);
        if (factors == null) {
            factors = factors(value);
            cache = new OneValueCache(value, factors);
        }

        encodeIntoResponse(res, factors);
    }

    private void encodeIntoResponse(final ServletResponse res, final int[] factors) throws IOException {
        for (int factor : factors) {
            res.getOutputStream().println(factor);
        }
    }

    private Integer extractFromRequest(final ServletRequest req) {
        return 60;
    }

    public int[] factors(final int value){
        return IntStream.range(0, value)
                .filter(intValue -> value % intValue == 0)
                .toArray();
    }



    @Override
    public void init(ServletConfig config) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
