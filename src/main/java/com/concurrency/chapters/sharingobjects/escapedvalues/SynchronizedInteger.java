package com.concurrency.chapters.sharingobjects.escapedvalues;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

@ThreadSafe
public class SynchronizedInteger {
    @GuardedBy("this") private int value;

    // if getter will be without sync it's still possible to see old value in other thread
    public synchronized int getValue() {
        return value;
    }

    public synchronized void setValue(int value) {
        this.value = value;
    }
}
