package com.concurrency.chapters.sharingobjects.escapedvalues;

import com.concurrency.tech.NotThreadSafe;

@NotThreadSafe("Stale value - After set other thread can see old value through the get")
public class MutableInteger {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
