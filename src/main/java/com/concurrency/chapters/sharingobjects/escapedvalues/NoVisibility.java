package com.concurrency.chapters.sharingobjects.escapedvalues;

public class NoVisibility {

    private static boolean ready;
    private static int number;

    public static class ReaderThread extends Thread {
        public void run(){
            while (!ready) {
                Thread.yield();
            }
            System.out.println(number);
        }
    }

    public static void main(String[] args){
        new NoVisibility.ReaderThread().start();
        number = 42;
        ready = true;
    }
}
