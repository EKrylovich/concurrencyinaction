package com.concurrency.chapters.sharingobjects.escapedvalues;

import java.util.HashSet;
import java.util.Set;

public class EscapedValues {

    public static Set<MutableInteger> knownSecrets;

    public void initialize(){
        knownSecrets = new HashSet<>();
    }



    private String[] states = {"AK", "AL", "LA"};

    public String[] getStates(){
        return states;
    }

}
