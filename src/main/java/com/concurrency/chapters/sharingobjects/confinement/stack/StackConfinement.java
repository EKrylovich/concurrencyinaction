package com.concurrency.chapters.sharingobjects.confinement.stack;

import jdk.nashorn.internal.codegen.types.ArrayType;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public class StackConfinement {

    private Ark ark;

    public int loadTheArk(final Collection<Animal> candidates) {
        SortedSet<Animal> animals;
        int numPairs = 0;
        Animal candidate = null;

        // animals confinement to the method
        animals = new TreeSet<>(new SpecialGenderComparator());
        animals.addAll(candidates);
        for (Animal animal : animals) {
            if (candidate == null || !candidate.isPotentialMate(animal)) {
                candidate = animal;
            } else {
                ark.load(new AnimalPair(candidate, animal));
                ++numPairs;
                candidate = null;
            }
        }

        return numPairs;

    }
}
