package com.concurrency.chapters.sharingobjects.confinement.threadlocal;

public class ConnectionProvider {

    private static ThreadLocal<Connection> connectionHolder = ThreadLocal.withInitial(Connection::new);

    public static Connection getConnection(){
        return connectionHolder.get();
    }
}
