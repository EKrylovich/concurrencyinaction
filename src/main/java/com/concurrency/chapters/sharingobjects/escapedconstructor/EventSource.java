package com.concurrency.chapters.sharingobjects.escapedconstructor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public class EventSource extends Thread{

    private final BlockingQueue<EventListener> listeners = new LinkedBlockingQueue<>();

    @Override
    public void run() {
        while (true) {
            try {
                listeners.take().onEvent(null);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    public void registerListener(EventListener eventListener) {
        this.listeners.add(eventListener);
    }
}
