package com.concurrency.chapters.sharingobjects.escapedconstructor;

import java.time.LocalDateTime;

public class SafeListener {

    private final EventListener listener;
    private final int num;

    public SafeListener() {
        listener = this::doSomething;
        num = 42;
    }

    public void doSomething(final Event event){
        if (num != 42) {
            System.out.println("Race condition detected at: " + LocalDateTime.now());
        }
    }

    public static SafeListener newInstance(final EventSource eventSource){
        final SafeListener safeListener = new SafeListener();
        eventSource.registerListener(safeListener.listener);
        return safeListener;
    }
}
