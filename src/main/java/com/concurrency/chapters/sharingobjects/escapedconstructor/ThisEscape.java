package com.concurrency.chapters.sharingobjects.escapedconstructor;


import java.time.LocalDateTime;

public class ThisEscape {

    private final int num;

    public ThisEscape(final EventSource source) {
        source.registerListener(this::doSomething);
        num = 42;
    }

    public void doSomething(final Event event){
        if (num != 42) {
            System.out.println("Race condition detected at: " + LocalDateTime.now());
        }
    }

}
