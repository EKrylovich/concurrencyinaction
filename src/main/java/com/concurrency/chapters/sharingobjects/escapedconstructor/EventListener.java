package com.concurrency.chapters.sharingobjects.escapedconstructor;

public interface EventListener {
    void onEvent(Event event);
}
