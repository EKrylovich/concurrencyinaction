package com.concurrency.chapters.buildingblocks.synchronizers;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CellularAutomata {
    private final Board mainBoard;
    private final CyclicBarrier cyclicBarrier;
    private final Worker[] workers;


    public CellularAutomata(final Board board) {
        this.mainBoard = board;
        final int count = Runtime.getRuntime().availableProcessors();
        this.cyclicBarrier = new CyclicBarrier(count, mainBoard::commitNewValues);
        this.workers = new Worker[count];
        for (int i = 0; i < count; i++){
            workers[i] = new Worker(mainBoard.getSubBoard(count, i));
        }
    }

    private class Worker implements Runnable{
        private final Board board;

        public Worker(final Board board){
            this.board = board;
        }

        @Override
        public void run() {
            while(!board.hasConverged()){
                for (int x = 0; x < board.getMaxX(); x++) {
                    for (int y = 0; y < board.getMaxY(); y++) {
                        board.setNewValue(x, y, computeValue(x, y));
                    }
                }
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    return;
                }
            }
        }
    }

    public void start(){
        for (int i = 0; i < workers.length; i++){
            new Thread(workers[i]).start();
            mainBoard.waitForConvergence();
        }
    }

    private int computeValue(int x, int y) {
        return 0;
    }


}
