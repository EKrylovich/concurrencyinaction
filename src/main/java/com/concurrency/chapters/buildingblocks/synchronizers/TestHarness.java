package com.concurrency.chapters.buildingblocks.synchronizers;

import java.util.concurrent.CountDownLatch;

public class TestHarness {

    public long timeTasks(final int nThreads, final Runnable task) throws InterruptedException{
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        for (int i = 0; i < nThreads; i++) {
            Thread thread = new Thread(() -> {
                try {
                    startGate.await();
                    try {
                        task.run();
                    } finally {
                        endGate.countDown();
                    }
                } catch (InterruptedException ignored) {
                }
            });
            thread.start();
        }

        final long start = System.nanoTime();
        startGate.countDown();
        endGate.await();
        final long end = System.nanoTime();
        return end - start;
    }
}
