package com.concurrency.chapters.buildingblocks.synchronizers;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Preloader {
    private final FutureTask<ProductInfo> future = new FutureTask<>(this::loadProductInfo);

    private ProductInfo loadProductInfo() throws InterruptedException {
        Thread.sleep(10000);
        return new ProductInfo();
    }

    private final Thread thread = new Thread(future);

    public void start(){
        thread.start();
    }

    public ProductInfo get() throws InterruptedException {
        try {
            return future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }
}
