package com.concurrency.chapters.buildingblocks.cache;

import com.concurrency.tech.GuardedBy;

import java.util.Map;
import java.util.concurrent.*;

public class Memoizer<A, V> implements Computable<A, V>{
    @GuardedBy("this")
    private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
    private final Computable<A, V> function;

    public Memoizer(final Computable<A, V> function) {
        this.function = function;
    }


    @Override
    public V compute(final A input) throws InterruptedException {
        while (true) {
            Future<V> future = cache.get(input);
            if (future == null) {
                final Callable<V> eval = () -> function.compute(input);
                final FutureTask<V> futureTask = new FutureTask<>(eval);
                future = cache.putIfAbsent(input, future);
                if (future == null) {
                    future = futureTask;
                    futureTask.run();
                }
            }
            try {
                return future.get();
            } catch (CancellationException exp) {
                cache.remove(input, future);
            } catch (ExecutionException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }
}
