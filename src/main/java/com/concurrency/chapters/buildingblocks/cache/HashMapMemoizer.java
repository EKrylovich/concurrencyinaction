package com.concurrency.chapters.buildingblocks.cache;

import com.concurrency.tech.GuardedBy;

import java.util.HashMap;
import java.util.Map;

//One thread at a time can run computation
public class HashMapMemoizer<A, V> implements Computable<A, V>{
    @GuardedBy("this")
    private final Map<A, V> cache = new HashMap<>();
    private final Computable<A, V> function;

    public HashMapMemoizer(final Computable<A, V> function) {
        this.function = function;
    }


    @Override
    public synchronized V compute(final A input) throws InterruptedException {
        V result = cache.get(input);
        if (result == null) {
            result = function.compute(input);
            cache.put(input, result);
        }
        return result;
    }
}
