package com.concurrency.chapters.buildingblocks.cache;

public class ExpensiveFunction implements Computable<String, Integer> {
    @Override
    public Integer compute(String input) throws InterruptedException {
        Thread.sleep(10000);
        return 0;
    }
}
