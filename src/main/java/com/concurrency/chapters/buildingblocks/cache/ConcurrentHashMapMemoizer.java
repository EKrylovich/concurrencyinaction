package com.concurrency.chapters.buildingblocks.cache;

import com.concurrency.tech.GuardedBy;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//Still there is a window when two the same computations can start in parallel
public class ConcurrentHashMapMemoizer<A, V> implements Computable<A, V>{
    @GuardedBy("this")
    private final Map<A, V> cache = new ConcurrentHashMap<>();
    private final Computable<A, V> function;

    public ConcurrentHashMapMemoizer(final Computable<A, V> function) {
        this.function = function;
    }


    @Override
    public V compute(final A input) throws InterruptedException {
        V result = cache.get(input);
        if (result == null) {
            result = function.compute(input);
            cache.put(input, result);
        }
        return result;
    }
}
