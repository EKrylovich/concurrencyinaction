package com.concurrency.chapters.buildingblocks.cache;

import com.concurrency.tech.GuardedBy;

import java.util.Map;
import java.util.concurrent.*;

//But still there is a small window for check-then-act
public class ConcurrentFutureHashMapMemoizer<A, V> implements Computable<A, V>{
    @GuardedBy("this")
    private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
    private final Computable<A, V> function;

    public ConcurrentFutureHashMapMemoizer(final Computable<A, V> function) {
        this.function = function;
    }


    @Override
    public V compute(final A input) throws InterruptedException {
        Future<V> future = cache.get(input);
        if (future == null) {
            final Callable<V> eval = () -> function.compute(input);
            final FutureTask<V> futureTask = new FutureTask<>(eval);
            future = futureTask;
            cache.put(input, future);
            futureTask.run();
        }
        try {
            return future.get();
        } catch (ExecutionException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
