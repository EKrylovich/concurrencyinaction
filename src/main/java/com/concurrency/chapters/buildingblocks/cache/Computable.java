package com.concurrency.chapters.buildingblocks.cache;

public interface Computable<A, V> {
    V compute(A input) throws InterruptedException;
}
