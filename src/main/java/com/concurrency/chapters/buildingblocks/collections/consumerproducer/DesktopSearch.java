package com.concurrency.chapters.buildingblocks.collections.consumerproducer;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DesktopSearch {
    public static void startIndexing(final File[] roots){
        final BlockingQueue<File> queue = new LinkedBlockingQueue<>(100);
        final FileFilter fileFilter = pathname -> true;
        for (File root : roots) {
            new Thread(new FileCrawler(queue, fileFilter, root)).start();
        }

        for (int i = 0; i < 10; i++){
            new Thread(new Indexer(queue)).start();
        }
    }
}
