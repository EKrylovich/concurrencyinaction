package com.concurrency.chapters.buildingblocks.collections;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.NotThreadSafe;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@NotThreadSafe("iteration and incorrect lock object is used")
public class HiddenIterator {

    @GuardedBy("this")
    private final Set<Integer> set = new HashSet<>();

    public synchronized void add(final Integer i) {
        set.add(i);
    }

    public synchronized void remove(final Integer i) {
        set.remove(i);
    }

    public void addTenThings(){
        final Random random = new Random();
        for (int i = 0; i < 10; i++){
            add(random.nextInt());
        }

        System.out.println("Debug: added 10 elements to " + set);
    }
}
