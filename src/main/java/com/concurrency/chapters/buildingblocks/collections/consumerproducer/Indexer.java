package com.concurrency.chapters.buildingblocks.collections.consumerproducer;

import java.io.File;
import java.util.concurrent.BlockingQueue;

public class Indexer implements Runnable{
    private final BlockingQueue<File> queue;

    public Indexer(final BlockingQueue<File> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true){
                indexFile(queue.take());
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void indexFile(File take) {

    }
}
