package com.concurrency.chapters.buildingblocks.collections;

import com.concurrency.tech.NotThreadSafe;
import com.concurrency.tech.ThreadSafe;

import java.util.Vector;

public class VectorCompound {

    @NotThreadSafe("check-then-act")
    public static <T> T getLast(final Vector<T> list){
        int lastIndex = list.size() - 1;
        return list.get(lastIndex);
    }

    @ThreadSafe
    public static <T> T deleteLast(final Vector<T> list){
        synchronized (list) {
            int lastIndex = list.size() - 1;
            return list.remove(lastIndex);
        }
    }
}
