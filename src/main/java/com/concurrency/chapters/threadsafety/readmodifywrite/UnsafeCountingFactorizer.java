package com.concurrency.chapters.threadsafety.readmodifywrite;

import com.concurrency.tech.NotThreadSafe;

import javax.servlet.*;
import java.io.IOException;
import java.util.stream.IntStream;

@NotThreadSafe("Race condition - read-modify-write issue")
public class UnsafeCountingFactorizer implements Servlet {

    private long count = 0;


    @Override
    public void service(final ServletRequest req, final ServletResponse res) throws IOException {
        final int value = extractFromRequest(req);
        final int[] factors = factors(value);

        ++count;

        encodeIntoResponse(res, factors);
    }

    private void encodeIntoResponse(final ServletResponse res, final int[] factors) throws IOException {
        for (int factor : factors) {
            res.getOutputStream().println(factor);
        }
    }

    private int extractFromRequest(final ServletRequest req) {
        return 60;
    }

    public int[] factors(final int value){
        return IntStream.range(0, value)
                .filter(intValue -> value % intValue == 0)
                .toArray();
    }

    @Override
    public void init(final ServletConfig config) throws ServletException {
        // nothing to do
    }

    @Override
    public ServletConfig getServletConfig() {
        // nothing to do
        return null;
    }

    @Override
    public String getServletInfo() {
        // nothing to do
        return null;
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
