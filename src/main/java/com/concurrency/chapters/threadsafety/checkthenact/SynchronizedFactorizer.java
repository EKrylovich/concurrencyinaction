package com.concurrency.chapters.threadsafety.checkthenact;

import com.concurrency.tech.GuardedBy;
import com.concurrency.tech.ThreadSafe;

import javax.servlet.*;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@ThreadSafe("But bad disign. Prevent using it in parallel")
public class SynchronizedFactorizer implements Servlet {

    @GuardedBy("this") private Integer lastNumber;
    @GuardedBy("this") private Integer[] lastFactors;

    @Override
    public synchronized void service(final ServletRequest req, final ServletResponse res) throws IOException {
        final Integer value = extractFromRequest(req);

        if (value.equals(lastNumber)) {
            encodeIntoResponse(res, lastFactors);
        } else {
            final Integer[] factors = factors(value);
            lastNumber = value;
            lastFactors = factors;
            encodeIntoResponse(res, factors);
        }

    }

    private void encodeIntoResponse(final ServletResponse res, final Integer[] factors) throws IOException {
        for (Integer factor : factors) {
            res.getOutputStream().println(factor);
        }
    }

    private int extractFromRequest(final ServletRequest req) {
        return 60;
    }

    public Integer[] factors(final int value){
        return IntStream.range(0, value)
                .filter(intValue -> value % intValue == 0)
                .boxed()
                .collect(Collectors.toList())
                .toArray(new Integer[]{});
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        // nothing to do
    }

    @Override
    public ServletConfig getServletConfig() {
        // nothing to do
        return null;
    }

    @Override
    public String getServletInfo() {
        // nothing to do
        return null;
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
