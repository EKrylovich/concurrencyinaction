package com.concurrency.chapters.threadsafety.checkthenact;

import com.concurrency.tech.NotThreadSafe;

import javax.servlet.*;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@NotThreadSafe("Race condition")
public class UnsafeCachingFactorizer implements Servlet {



    private final AtomicReference<Integer> lastNumber = new AtomicReference<>();
    private final AtomicReference<Integer[]> lastFactors = new AtomicReference<>();


    @Override
    public void service(final ServletRequest req, final ServletResponse res) throws IOException {
        final Integer value = extractFromRequest(req);

        if (value.equals(lastNumber.get())) {
            encodeIntoResponse(res, lastFactors.get());
        } else {
            final Integer[] factors = factors(value);
            lastNumber.set(value);
            lastFactors.set(factors);
            encodeIntoResponse(res, factors);
        }

    }

    private void encodeIntoResponse(final ServletResponse res, final Integer[] factors) throws IOException {
        for (Integer factor : factors) {
            res.getOutputStream().println(factor);
        }
    }

    private int extractFromRequest(final ServletRequest req) {
        return 60;
    }

    public Integer[] factors(final int value){
        return IntStream.range(0, value)
                .filter(intValue -> value % intValue == 0)
                .boxed()
                .collect(Collectors.toList())
                .toArray(new Integer[]{});
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        // nothing to do
    }

    @Override
    public ServletConfig getServletConfig() {
        // nothing to do
        return null;
    }

    @Override
    public String getServletInfo() {
        // nothing to do
        return null;
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
