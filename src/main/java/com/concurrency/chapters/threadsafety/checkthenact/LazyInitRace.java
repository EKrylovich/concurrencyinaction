package com.concurrency.chapters.threadsafety.checkthenact;

import com.concurrency.chapters.threadsafety.readmodifywrite.UnsafeCountingFactorizer;
import com.concurrency.tech.NotThreadSafe;

@NotThreadSafe("Race condition - check-then-act issue")
public class LazyInitRace {
    private UnsafeCountingFactorizer instance = null;

    public UnsafeCountingFactorizer getInstance(){
        if(instance == null) {
            instance = new UnsafeCountingFactorizer();
        }
        return instance;
    }

}
