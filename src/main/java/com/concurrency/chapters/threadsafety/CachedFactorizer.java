package com.concurrency.chapters.threadsafety;

import com.concurrency.tech.GuardedBy;

import javax.servlet.*;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CachedFactorizer implements Servlet {


    @GuardedBy("this") private Integer lastNumber;
    @GuardedBy("this") private Integer[] lastFactors;
    @GuardedBy("this") private long hits;
    @GuardedBy("this") private long cachedHits;


    public synchronized long hits(){
        return hits;
    }

    public synchronized double cacheHitRatio(){
        return (double) cachedHits / (double) hits;
    }


    @Override
    public synchronized void service(final ServletRequest req, final ServletResponse res) throws IOException {
        final Integer value = extractFromRequest(req);
        Integer[] factors = {};

        synchronized (this) {
            hits++;
            if (value.equals(lastNumber)) {
                cachedHits++;
                factors = lastFactors.clone();
            }
        }

        if (factors == null) {
            factors = factors(value);
            synchronized (this) {
                lastNumber = value;
                lastFactors = factors;
            }
            encodeIntoResponse(res, factors);
        }

    }

    private void encodeIntoResponse(final ServletResponse res, final Integer[] factors) throws IOException {
        for (Integer factor : factors) {
            res.getOutputStream().println(factor);
        }
    }

    private int extractFromRequest(final ServletRequest req) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 60;
    }

    public Integer[] factors(final int value){
        return IntStream.range(0, value)
                .filter(intValue -> value % intValue == 0)
                .boxed()
                .collect(Collectors.toList())
                .toArray(new Integer[]{});
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        // nothing to do
    }

    @Override
    public ServletConfig getServletConfig() {
        // nothing to do
        return null;
    }

    @Override
    public String getServletInfo() {
        // nothing to do
        return null;
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
